package gregoiredx.demo.rankings

import dagger.Component
import gregoiredx.demo.rankings.health.ScoreDaoHealthCheck
import gregoiredx.demo.rankings.resources.PlayerResource
import gregoiredx.demo.rankings.resources.PlayersResource
import javax.inject.Singleton

@Component(modules = [Conf::class])
@Singleton
interface Components {

    fun playerResource(): PlayerResource
    fun playersResource(): PlayersResource
    fun scoreDaoHealthCheck(): ScoreDaoHealthCheck

}
