package gregoiredx.demo.rankings

import dagger.Module
import dagger.Provides
import gregoiredx.demo.rankings.dao.DynamoDbScoreDao
import gregoiredx.demo.rankings.dao.MemoryScoreDao
import gregoiredx.demo.rankings.dao.ScoreDao
import io.dropwizard.Configuration
import javax.inject.Singleton

@Module
class Conf(private val database: String = "dynamodb", private val dynamoDb: DynamoDbConf? = null) : Configuration() {

    @Provides
    @Singleton
    fun providePlayerDao(): ScoreDao {
        return when (database) {
            "dynamodb" -> dynamoDb?.let { DynamoDbScoreDao(it) } ?: throw Exception("Missing dynamodb configuration")
            "memory" -> MemoryScoreDao()
            else -> throw Exception("Unexpected database: $database")
        }
    }

}

class DynamoDbConf(
        val endpoint: String, val region: String,
        val accessKey: String, val secretKey: String,
        val scoresTable: String = "Scores"
)
