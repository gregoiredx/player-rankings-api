package gregoiredx.demo.rankings.dao

typealias PlayerScore = Pair<String, Int>

interface ScoreDao {
    fun addPoints(name: String, points: Int): Boolean
    fun find(name: String): Int?
    fun getHigherThan(minScore: Int): Sequence<Int>
    fun getAll(): Sequence<PlayerScore>
    fun clear()
    fun healthCheck()
}