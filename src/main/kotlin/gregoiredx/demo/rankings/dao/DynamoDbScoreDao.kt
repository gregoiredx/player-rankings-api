package gregoiredx.demo.rankings.dao

import com.amazonaws.auth.AWSStaticCredentialsProvider
import com.amazonaws.auth.BasicAWSCredentials
import com.amazonaws.client.builder.AwsClientBuilder.EndpointConfiguration
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder.standard
import com.amazonaws.services.dynamodbv2.document.DynamoDB
import com.amazonaws.services.dynamodbv2.document.Expected
import com.amazonaws.services.dynamodbv2.document.Item
import com.amazonaws.services.dynamodbv2.document.Table
import com.amazonaws.services.dynamodbv2.document.spec.GetItemSpec
import com.amazonaws.services.dynamodbv2.document.spec.ScanSpec
import com.amazonaws.services.dynamodbv2.document.spec.UpdateItemSpec
import com.amazonaws.services.dynamodbv2.document.utils.NameMap
import com.amazonaws.services.dynamodbv2.document.utils.ValueMap
import com.amazonaws.services.dynamodbv2.model.*
import gregoiredx.demo.rankings.DynamoDbConf
import org.slf4j.LoggerFactory
import javax.inject.Singleton


private val LOGGER = LoggerFactory.getLogger(DynamoDbScoreDao::class.java)

private const val NAME_ATTRIBUTE = "name"
private const val SCORE_ATTRIBUTE = "score"

@Singleton
class DynamoDbScoreDao(conf: DynamoDbConf) : ScoreDao {

    private val dynamoDB = DynamoDB(standard()
            .withCredentials(AWSStaticCredentialsProvider(
                    BasicAWSCredentials(conf.accessKey, conf.secretKey))
            )
            .withEndpointConfiguration(
                    EndpointConfiguration(conf.endpoint, conf.region)
            ).build()
    )

    private val table: Table = dynamoDB.getTable(conf.scoresTable)

    init {
        createTable()
    }

    override fun addPoints(name: String, points: Int): Boolean {
        return try {
            createScore(name, points)
            true
        } catch (e: ConditionalCheckFailedException) {
            updatePoints(name, points)
            false
        }
    }

    override fun find(name: String): Int? {
        val spec = GetItemSpec().withPrimaryKey(NAME_ATTRIBUTE, name)
        return table.getItem(spec)?.getInt(SCORE_ATTRIBUTE)
    }

    override fun getHigherThan(minScore: Int): Sequence<Int> {
        val spec = ScanSpec()
                .withProjectionExpression(SCORE_ATTRIBUTE)
                .withFilterExpression("$SCORE_ATTRIBUTE > :minScore")
                .withValueMap(ValueMap().withInt(":minScore", minScore))
        return table.scan(spec).asSequence().map { it.getInt(SCORE_ATTRIBUTE) }
    }

    override fun getAll(): Sequence<PlayerScore> {
        val spec = ScanSpec()
                .withProjectionExpression("#nm, $SCORE_ATTRIBUTE") // name is a reserved keyword
                .withNameMap(NameMap().with("#nm", NAME_ATTRIBUTE))
        return table.scan(spec).asSequence().map {
            PlayerScore(it.getString(NAME_ATTRIBUTE), it.getInt(SCORE_ATTRIBUTE))
        }
    }

    override fun clear() {
        deleteTable()
        createTable()
    }

    override fun healthCheck() {
        table.describe() // will throw if DynamoDB is not accessible
    }

    private fun createScore(name: String, score: Int) {
        val item = Item().withPrimaryKey(NAME_ATTRIBUTE, name).with(SCORE_ATTRIBUTE, score)
        table.putItem(item, Expected(NAME_ATTRIBUTE).notExist())
    }

    private fun updatePoints(name: String, points: Int) {
        val spec: UpdateItemSpec = UpdateItemSpec()
                .withPrimaryKey(NAME_ATTRIBUTE, name)
                .withUpdateExpression("set $SCORE_ATTRIBUTE = $SCORE_ATTRIBUTE + :points")
                .withValueMap(ValueMap().withNumber(":points", points))
                .withReturnValues(ReturnValue.NONE)
        table.updateItem(spec)
    }

    private fun createTable() {
        try {
            dynamoDB.createTable(
                    table.tableName,
                    listOf(KeySchemaElement(NAME_ATTRIBUTE, KeyType.HASH)),
                    listOf(AttributeDefinition(NAME_ATTRIBUTE, ScalarAttributeType.S)),
                    ProvisionedThroughput(10L, 10L)
            ).waitForActive()
        } catch (e: ResourceInUseException) {
            LOGGER.info("Table already exists")
        }
    }

    private fun deleteTable() {
        try {
            table.delete()
            table.waitForDelete()
        } catch (e: ResourceNotFoundException) {
            LOGGER.info("Table does not exists, nothing to delete")
        }
    }

}