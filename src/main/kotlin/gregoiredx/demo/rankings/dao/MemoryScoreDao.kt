package gregoiredx.demo.rankings.dao

import java.util.concurrent.ConcurrentHashMap
import javax.inject.Singleton

@Singleton
class MemoryScoreDao : ScoreDao {

    private val scoresByName = ConcurrentHashMap<String, Int>()

    override fun addPoints(name: String, points: Int): Boolean {
        val existingScore = scoresByName[name]
        if (existingScore != null) {
            scoresByName[name] = existingScore + points
            return false
        }
        scoresByName[name] = points
        return true
    }

    override fun find(name: String): Int? {
        return scoresByName[name]
    }

    override fun getHigherThan(minScore: Int): Sequence<Int> {
        return scoresByName.asSequence().map { it.value }.filter { it > minScore }
    }

    override fun getAll(): Sequence<PlayerScore> {
        return scoresByName.asSequence().map { PlayerScore(it.key, it.value) }
    }

    override fun clear() {
        scoresByName.clear()
    }

    override fun healthCheck() {}

}