package gregoiredx.demo.rankings.model

data class Player(val name: String, val score: Int, val ranking: Int)
