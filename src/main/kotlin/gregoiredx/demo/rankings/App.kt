package gregoiredx.demo.rankings

import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import io.dropwizard.Application
import io.dropwizard.assets.AssetsBundle
import io.dropwizard.bundles.redirect.PathRedirect
import io.dropwizard.bundles.redirect.RedirectBundle
import io.dropwizard.configuration.EnvironmentVariableSubstitutor
import io.dropwizard.configuration.SubstitutingSourceProvider
import io.dropwizard.setup.Bootstrap
import io.dropwizard.setup.Environment
import io.swagger.v3.jaxrs2.integration.resources.OpenApiResource


class App : Application<Conf>() {

    override fun getName(): String {
        return "Player rankings API"
    }

    override fun initialize(bootstrap: Bootstrap<Conf>) {
        bootstrap.objectMapper?.registerKotlinModule()
        bootstrap.configurationSourceProvider = SubstitutingSourceProvider(
                bootstrap.configurationSourceProvider, EnvironmentVariableSubstitutor(false)
        )
        bootstrap.addBundle(AssetsBundle("/swagger/", "/swagger", "index.html"))
        bootstrap.addBundle(RedirectBundle(PathRedirect("/", "/swagger/")))
    }

    override fun run(configuration: Conf, environment: Environment) {
        val components = DaggerComponents.builder().conf(configuration).build()
        environment.healthChecks().register("scoreDao", components.scoreDaoHealthCheck())
        environment.jersey().register(components.playerResource())
        environment.jersey().register(components.playersResource())
        environment.jersey().register(OpenApiResource())
    }

}

fun main(args: Array<String>) {
    App().run(*args)
}
