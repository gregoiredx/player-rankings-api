package gregoiredx.demo.rankings.health

import com.codahale.metrics.health.HealthCheck
import com.codahale.metrics.health.HealthCheck.Result.healthy
import com.codahale.metrics.health.HealthCheck.Result.unhealthy
import gregoiredx.demo.rankings.dao.ScoreDao
import javax.inject.Inject

class ScoreDaoHealthCheck @Inject constructor(private val scoreDao: ScoreDao) : HealthCheck() {

    override fun check(): Result = try {
        scoreDao.healthCheck()
        healthy()
    } catch (e: Throwable) {
        unhealthy(e)
    }

}


