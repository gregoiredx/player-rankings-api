package gregoiredx.demo.rankings.resources

import com.codahale.metrics.annotation.Timed
import gregoiredx.demo.rankings.model.Player
import gregoiredx.demo.rankings.repositories.PlayerRepository
import javax.inject.Inject
import javax.inject.Singleton
import javax.ws.rs.*
import javax.ws.rs.core.MediaType.APPLICATION_FORM_URLENCODED
import javax.ws.rs.core.MediaType.APPLICATION_JSON
import javax.ws.rs.core.Response
import javax.ws.rs.core.UriBuilder

@Path("/players")
@Singleton
class PlayersResource @Inject constructor(private val repository: PlayerRepository) {

    @GET
    @Produces(APPLICATION_JSON)
    @Timed
    fun getAllSortedByRanking(): List<Player> {
        return repository.getAllSortedByRanking()
    }


    @POST
    @Consumes(APPLICATION_FORM_URLENCODED)
    @Timed
    fun create(@FormParam("name") name: String): Response {
        val location = UriBuilder.fromResource(PlayerResource::class.java).build(name)
        return if (repository.create(name)) {
            Response.created(location).build()
        } else {
            Response.noContent().location(location).build()
        }
    }

    @DELETE
    @Timed
    fun deleteAll() {
        repository.deleteAll()
    }

}