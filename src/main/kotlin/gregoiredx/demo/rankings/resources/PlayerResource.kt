package gregoiredx.demo.rankings.resources

import com.codahale.metrics.annotation.Timed
import gregoiredx.demo.rankings.model.Player
import gregoiredx.demo.rankings.repositories.PlayerRepository
import javax.inject.Inject
import javax.inject.Singleton
import javax.ws.rs.*
import javax.ws.rs.core.MediaType.APPLICATION_FORM_URLENCODED
import javax.ws.rs.core.MediaType.APPLICATION_JSON
import javax.ws.rs.core.Response.Status.NOT_FOUND

@Path("/players/{name}")
@Singleton
class PlayerResource @Inject constructor(private val repository: PlayerRepository) {

    @GET
    @Produces(APPLICATION_JSON)
    @Timed
    fun get(@PathParam("name") name: String): Player {
        return repository.find(name) ?: throw WebApplicationException(NOT_FOUND)
    }

    @POST
    @Consumes(APPLICATION_FORM_URLENCODED)
    @Timed
    fun addPoints(@PathParam("name") name: String, @FormParam("points") points: Int) {
        repository.addPoints(name, points)
    }

}