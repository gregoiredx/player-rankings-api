package gregoiredx.demo.rankings.repositories

import gregoiredx.demo.rankings.dao.ScoreDao
import gregoiredx.demo.rankings.model.Player
import javax.inject.Inject

class PlayerRepository @Inject constructor(private val scoreDao: ScoreDao) {

    fun create(name: String) = scoreDao.addPoints(name, 0)

    fun find(name: String): Player? = scoreDao.find(name)?.let { score ->
        val ranking = scoreDao.getHigherThan(score).distinct().count() + 1
        return Player(name, score, ranking)
    }

    fun addPoints(name: String, points: Int) {
        scoreDao.addPoints(name, points)
    }

    fun getAllSortedByRanking(): List<Player> {
        var currentRank = 0
        var currentScore = Int.MAX_VALUE
        val sortedPlayers = mutableListOf<Player>()
        for ((name, score) in scoreDao.getAll().sortedByDescending { it.second }) {
            if (score < currentScore) {
                currentRank += 1
                currentScore = score
            }
            sortedPlayers.add(Player(name, score, currentRank))
        }
        return sortedPlayers
    }

    fun deleteAll() = scoreDao.clear()

}
