package gregoiredx.demo.rankings.repositories

import gregoiredx.demo.rankings.dao.PlayerScore
import gregoiredx.demo.rankings.dao.ScoreDao
import gregoiredx.demo.rankings.model.Player
import io.mockk.every
import io.mockk.mockk
import org.assertj.core.api.Assertions.assertThat
import org.junit.Before
import org.junit.Test
import kotlin.test.assertNotNull

class PlayerRepositoryTest {

    private lateinit var scoreDao: ScoreDao

    private lateinit var repository: PlayerRepository

    @Before
    fun setUp() {
        scoreDao = mockk()
        repository = PlayerRepository(scoreDao)
    }

    @Test
    fun find() {
        every { scoreDao.find("joe") } returns 123
        every { scoreDao.getHigherThan(123) } returns sequenceOf(234, 523, 342, 523)

        val found = repository.find("joe")

        assertNotNull(found)
        assertThat(found.name).isEqualTo("joe")
        assertThat(found.score).isEqualTo(123)
        assertThat(found.ranking).isEqualTo(4)
    }

    @Test
    fun getAll() {
        every { scoreDao.getAll() } returns sequenceOf(
                PlayerScore("max", 123), PlayerScore("joe", 342), PlayerScore("zoe", 12), PlayerScore("bob", 123)
        )

        val all = repository.getAllSortedByRanking()

        assertThat(all).containsExactly(
                Player("joe", 342, 1),
                Player("max", 123, 2),
                Player("bob", 123, 2),
                Player("zoe", 12, 3)
        )
    }

}