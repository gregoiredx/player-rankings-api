package gregoiredx.demo.rankings.model

import gregoiredx.demo.rankings.JSON_MAPPER
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test

class PlayerSerializationTest {

    @Test
    fun serialize() {
        val person = Player("joe", 352, 2)

        val serialized = JSON_MAPPER.writeValueAsString(person)

        assertThat(serialized).isEqualTo(JSON_MAPPER.writeValueAsString(
                JSON_MAPPER.readTree("""{"name": "joe", "score": 352, "ranking": 2}"""))
        )
    }

    @Test
    fun deserialize() {
        val serialized = """{"name": "joe", "score": 352, "ranking": 2}"""

        val deserialized = JSON_MAPPER.readValue(serialized, Player::class.java)

        assertThat(deserialized).isEqualTo(Player("joe", 352, 2))
    }

}
