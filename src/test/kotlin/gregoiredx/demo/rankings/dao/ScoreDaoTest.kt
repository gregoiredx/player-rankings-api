package gregoiredx.demo.rankings.dao

import gregoiredx.demo.rankings.TEST_CONFIGURATION
import org.assertj.core.api.Assertions.assertThat
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.Parameterized
import org.junit.runners.Parameterized.Parameters


@RunWith(Parameterized::class)
class ScoreDaoTest(private val playerDao: ScoreDao) {

    companion object {

        @Parameters(name = "{0}")
        @JvmStatic
        fun configurations(): Collection<ScoreDao> {
            return listOf(MemoryScoreDao(), TEST_CONFIGURATION.providePlayerDao())
        }

    }

    @Before
    fun setUp() {
        playerDao.clear()
    }

    @Test
    fun create() {
        val created = playerDao.addPoints("joe", 3)

        assertThat(created).isTrue()
        assertThat(playerDao.find("joe")).isEqualTo(3)
    }

    @Test
    fun update() {
        playerDao.addPoints("joe", 3)

        val created = playerDao.addPoints("joe", 5)

        assertThat(created).isFalse()
        assertThat(playerDao.find("joe")).isEqualTo(8)
    }

    @Test
    fun getAll() {
        playerDao.addPoints("zoe", 324)
        playerDao.addPoints("max", 512)
        playerDao.addPoints("joe", 2)

        val all = playerDao.getAll()

        assertThat(all.toList()).containsExactlyInAnyOrder(
                PlayerScore("zoe", 324),
                PlayerScore("max", 512),
                PlayerScore("joe", 2)
        )
    }

    @Test
    fun getHigherThan() {
        playerDao.addPoints("zoe", 324)
        playerDao.addPoints("max", 512)
        playerDao.addPoints("joe", 2)

        val higherThan = playerDao.getHigherThan(15)

        assertThat(higherThan.toList()).containsExactlyInAnyOrder(324, 512)
    }

}
