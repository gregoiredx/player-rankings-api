package gregoiredx.demo.rankings

import gregoiredx.demo.rankings.model.Player
import org.assertj.core.api.Assertions.assertThat
import org.glassfish.jersey.client.ClientProperties.READ_TIMEOUT
import org.junit.Before
import org.junit.ClassRule
import org.junit.Test
import javax.ws.rs.client.Entity.form
import javax.ws.rs.core.Form
import javax.ws.rs.core.Response
import javax.ws.rs.core.Response.Status.*


class WebApiAcceptanceTest {

    @Before
    fun setUp() {
        assertThat(playersWebTarget().request().delete().status).isEqualTo(NO_CONTENT.statusCode)
    }

    @Test
    fun createPlayer() {
        val createResponse = callCreatePlayer("joe")

        assertThat(createResponse.status).isEqualTo(CREATED.statusCode)
        val locationResponse = client().target(createResponse.location).request().get()
        assertJsonPlayer(locationResponse, Player("joe", 0, 1))
    }

    @Test
    fun informsWhenReCreatingExistingPlayer() {
        callCreatePlayer("joe")

        val reCreateResponse = callCreatePlayer("joe")

        assertThat(reCreateResponse.status).isEqualTo(NO_CONTENT.statusCode)
    }

    @Test
    fun getPlayer() {
        callCreatePlayer("joe")

        callGetPlayerAndAssertJson(Player("joe", 0, 1))
    }

    @Test
    fun tryToGetUnknownPlayer() {
        val getResponse = callGetPlayer("joe")

        assertThat(getResponse.status).isEqualTo(NOT_FOUND.statusCode)
    }

    @Test
    fun addPoints() {
        callCreatePlayer("joe")

        val addPointsResponse = callAddPoints("joe", 3)

        assertThat(addPointsResponse.status).isEqualTo(NO_CONTENT.statusCode)
        callGetPlayerAndAssertJson(Player("joe", 3, 1))
    }

    @Test
    fun autoCreateWhenAddingPointsForTheFirstTime() {
        val addPointsResponse = callAddPoints("joe", 3)

        assertThat(addPointsResponse.status).isEqualTo(NO_CONTENT.statusCode)
        callGetPlayerAndAssertJson(Player("joe", 3, 1))
    }

    @Test
    fun addPointsMultipleTimes() {
        callAddPoints("joe", 3)
        callAddPoints("joe", -5)
        callAddPoints("joe", 534)

        callGetPlayerAndAssertJson(Player("joe", 532, 1))
    }

    @Test
    fun getAllSortedByRanking() {
        callAddPoints("joe", 3)
        callAddPoints("max", 534)
        callAddPoints("zoe", 128)

        val getAllResponse = playersWebTarget().request().get()

        val entity = getAllResponse.readEntity<Array<Player>>(Array<Player>::class.java)
        assertThat(entity).containsExactly(
                Player("max", 534, 1),
                Player("zoe", 128, 2),
                Player("joe", 3, 3)
        )
    }


    companion object {

        @ClassRule
        @JvmField
        val RULE = AppRule()

        private fun callGetPlayer(name: String) = playersWebTarget().path(name).request().get()

        private fun callCreatePlayer(name: String) = playersWebTarget().request()
                .post(form(Form("name", name)))

        private fun callAddPoints(name: String, points: Int) = playersWebTarget().path(name).request()
                .post(form(Form("points", points.toString())))

        private fun callGetPlayerAndAssertJson(player: Player) = assertJsonPlayer(callGetPlayer(player.name), player)

        private fun assertJsonPlayer(getResponse: Response, player: Player) {
            assertThat(getResponse.status).isEqualTo(OK.statusCode)
            val entity = getResponse.readEntity<Player>(Player::class.java)
            assertThat(entity).isEqualTo(player)
        }

        private fun playersWebTarget() = client().target("http://localhost:${RULE.localPort}").path("players")

        private fun client() = RULE.client().property(READ_TIMEOUT, 10000)
    }

}
