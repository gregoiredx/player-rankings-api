package gregoiredx.demo.rankings

import ch.qos.logback.classic.LoggerContext
import ch.qos.logback.classic.joran.JoranConfigurator
import com.fasterxml.jackson.core.JsonFactory
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory
import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import io.dropwizard.configuration.YamlConfigurationFactory
import io.dropwizard.jackson.Jackson
import io.dropwizard.jersey.validation.Validators
import io.dropwizard.testing.ResourceHelpers
import io.dropwizard.testing.junit.DropwizardAppRule
import org.slf4j.LoggerFactory
import java.io.File

private const val TEST_CONFIGURATION_FILE = "test.yml"
private const val LOGBACK_CONFIGURATION = "logback-test.xml"

val YAML_MAPPER = objectMapper(YAMLFactory())

val JSON_MAPPER = objectMapper()

private fun objectMapper(factory: JsonFactory? = null) = Jackson.newObjectMapper(factory).registerKotlinModule()

val TEST_CONFIGURATION: Conf =
        YamlConfigurationFactory(Conf::class.java, Validators.newValidator(), YAML_MAPPER, "dw")
                .build(File(ResourceHelpers.resourceFilePath(TEST_CONFIGURATION_FILE)))

class AppRule : DropwizardAppRule<Conf>(App::class.java, ResourceHelpers.resourceFilePath(TEST_CONFIGURATION_FILE)) {

    override fun after() {
        super.after()
        resetLogConfig()
    }

    private fun resetLogConfig() {
        val context = LoggerFactory.getILoggerFactory() as LoggerContext
        val configurator = JoranConfigurator()
        configurator.context = context
        context.reset()
        configurator.doConfigure(ResourceHelpers.resourceFilePath(LOGBACK_CONFIGURATION))
    }
}
