## Player rankings API


A simple web API demo using 
[Kotlin](https://kotlinlang.org), 
[Dropwizard](https://www.dropwizard.io), 
[DynamoDB](https://aws.amazon.com/fr/dynamodb/) 
(through [LocalStack](https://github.com/localstack/localstack)), 
[Dagger](https://google.github.io/dagger/) and
[OpenAPI](https://www.openapis.org)
(through [Swagger](https://swagger.io)).

### Dependencies

###### Java
Building and running the application requires a `JAVA_HOME` environment variable set to a [JDK 8](https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html) location.

###### LocalStack (optional, see 'Building and running')
If you want to test DynamoDB features, you need a [LocalStack](https://github.com/localstack/localstack) instance running on `localhost` with the default port for the DynamoDB service (`4569`).

You can launch one using: 
```shell
docker-compose up -d
```
If you want to use another host or port you can modify `conf/local.yml` and `src/test/resources/test.yml` 
or add `-Ddw.dynamoDb.endpoint=http://myendpoint:4242` to `JAVA_OPTS` and `MAVEN_CLI_OPTS` (see below).

###### ports

The application uses ports `9191` and `9192` by default. If you want to use other ports you can modify `conf/local.yml` or use `JAVA_OPTS` (see below).

### Building and running

Execute:
```shell
./build-and-start-local.sh
```

The script will run maven to package the application and launch the application with java.

To run without LocalStack, execute
```shell
 ./build-and-start-local.sh --no-dynamodb
```
To add options to the `mvnw` command, use the environment variable `MAVEN_CLI_OPTS`.

To add options to the `java` command, use the environment variable `JAVA_OPTS`.

To change log level in tests, use the environment variable `TEST_LOG_LEVEL` (defaults to `WARN`).

For example, you could skip maven clean, show tests `INFO` logs and launch the application on port `8081` with:<br>
```shell
MAVEN_CLI_OPTS="-Dmaven.clean.skip=true" TEST_LOG_LEVEL="INFO" JAVA_OPTS="-Ddw.server.applicationConnectors[0].port=8081" ./build-and-start-local.sh
```

### Exploring the API

#### Swagger UI

Open http://localhost:9191 on your favorite web browser.

#### [curl](https://curl.haxx.se/)

###### Create a player
```shell
curl http://locahost:9191/players -d "name=joe"
```
Calling multiple time is accepted but has no effect.

###### Add points to a player
```shell
curl http://locahost:9191/players/joe -d "points=42"
```
Adding points to an unknown player is accepted (this will create the player).

###### Get a player
```shell
curl http://locahost:9191/players/joe
```
###### Get all players sorted by ranking
```shell
curl http://locahost:9191/players
```
###### Delete all players
```shell
curl -X DELETE http://locahost:9191/players
```
### Admin resources

You can get admin resources at  http://localhost:9192.

### Notes

Calculating player rankings on each retrieval is probably not a good idea under heavy load.
We could use a web cache to reduce the load on GET requests to `/players` and `/players/{name}` 
and/or calculate rankings every x seconds and store the result in memory or in a dedicated table (this could be done by an external batch).
