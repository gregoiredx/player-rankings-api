#! /usr/bin/env bash

set -e

if [[ "$1" == "--no-dynamodb" ]]
then
    export MAVEN_CLI_OPTS="-Ddw.database=memory $MAVEN_CLI_OPTS"
    export JAVA_OPTS="-Ddw.database=memory $JAVA_OPTS"
fi

cd $(dirname $0)

set -x

TEST_LOG_LEVEL=${TEST_LOG_LEVEL:-WARN} ./mvnw ${MAVEN_CLI_OPTS} clean package

${JAVA_HOME}/bin/java ${JAVA_OPTS} -jar target/player-rankings-api-*.jar server conf/local.yml
